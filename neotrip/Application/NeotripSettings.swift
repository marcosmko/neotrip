//
//  NeotripSettings.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

func msleep(_ miliseconds: useconds_t) {
    usleep(miliseconds*1000)
}
func locale() -> String? {
    let pre = Locale.preferredLanguages
    return (pre.count > 0) ? pre[0] : nil
}

struct Constants {
    
    struct NeotripServer {
        static let baseURL: String = "http://api.localhost.com:3000/v1/"
    }
    
}
