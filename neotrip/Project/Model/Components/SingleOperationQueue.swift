//
//  OneOperationQueue.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 13/09/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class SingleOperationQueue: OperationQueue {
    
    override func addOperation(_ op: Operation) {
        cancelAllOperations()
        super.addOperation(op)
    }
    
}
