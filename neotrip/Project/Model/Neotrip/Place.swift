//
//  Place+CoreDataClass.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/27/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//
//

import CoreData

public class Place: NSManagedObject {

    @NSManaged public var name: String?
    
    convenience init() {
        let managedObjectContext: NSManagedObjectContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let entityDescription: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Place", in: managedObjectContext)!
        self.init(entity: entityDescription, insertInto: nil)
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Place> {
        return NSFetchRequest<Place>(entityName: "Place")
    }
    
}
