//
//  Transport+CoreDataClass.swift
//  
//
//  Created by Marcos Kobuchi on 9/27/17.
//
//

import CoreData

public class Transport: NSManagedObject {
    
    convenience init() {
        let managedObjectContext: NSManagedObjectContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let entityDescription = NSEntityDescription.entity(forEntityName: "Transport", in: managedObjectContext)!
        self.init(entity: entityDescription, insertInto: nil)
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transport> {
        return NSFetchRequest<Transport>(entityName: "Transport")
    }

}
