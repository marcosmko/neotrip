//
//  Travel+CoreDataClass.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 23/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//
//

import CoreData

public final class Travel: NSManagedObject {
    
    @NSManaged public var name: String?
    @NSManaged public var startDate: Date?
    @NSManaged public var duration: Double
    
    @NSManaged public var origin: City?
    @NSManaged public var cities: NSOrderedSet
    
    convenience init() {
        let managedObjectContext: NSManagedObjectContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let entityDescription = NSEntityDescription.entity(forEntityName: "Travel", in: managedObjectContext)!
        self.init(entity: entityDescription, insertInto: nil)
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Travel> {
        return NSFetchRequest<Travel>(entityName: "Travel")
    }
    
    func add(_ city: City) {
        super.add(city, forKey: "cities")
    }
    
    func remove(_ city: City) {
        super.remove(city, forKey: "cities")
    }

}
