//
//  City.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import CoreData
import CoreGraphics

public final class City: NSManagedObject {
    @NSManaged public var id: String
    @NSManaged public var googlePlaceId: String?
    
    @NSManaged public var name: String?
    @NSManaged public var fullDescription: String?
    
    @NSManaged public var places: NSOrderedSet
    @NSManaged public var transports: NSOrderedSet
    
    var geometry: Geometry?
    
    convenience init() {
        let managedObjectContext: NSManagedObjectContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let entityDescription: NSEntityDescription = NSEntityDescription.entity(forEntityName: "City", in: managedObjectContext)!
        self.init(entity: entityDescription, insertInto: nil)
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }
    
    func add(_ place: Place) {
        super.add(place, forKey: "places")
    }
    
    func add(_ transport: Transport) {
        super.add(transport, forKey: "transports")
    }
    
    func remove(_ place: Place) {
        super.remove(place, forKey: "places")
    }
    
    func remove(_ transport: Transport) {
        super.remove(transport, forKey: "transports")
    }
    
}

extension City: Codable {
    
    private enum CodingKeys: String, CodingKey { case id, googlePlaceId = "google_place_id", name, fullDescription = "full_description", geometry }
    
    public convenience init(from decoder: Decoder) throws {
        self.init()
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        googlePlaceId = try values.decodeIfPresent(String.self, forKey: .googlePlaceId)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        fullDescription = try values.decodeIfPresent(String.self, forKey: .fullDescription)
        geometry = try values.decodeIfPresent(Geometry.self, forKey: .geometry)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encodeIfPresent(googlePlaceId, forKey: .googlePlaceId)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(fullDescription, forKey: .fullDescription)
        try container.encodeIfPresent(geometry, forKey: .geometry)
    }
    
}

struct Geometry: Codable {
    var location: Location
    var viewport: Viewport
}

struct Location: Codable {
    var lat: Double
    var lng: Double
}

struct Viewport: Codable {
    var northeast: Location
    var southwest: Location
}
