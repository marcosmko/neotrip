//
//  Errors.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/24/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

enum NeotripError: Error {
    case DatabaseFailure
    case ServerFailure
    case LocationFailure
}
