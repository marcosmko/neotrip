//
//  FSContact.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSContact : Codable {
    
    // Facebook
    var facebook:String?
    var facebookName:String?
    var facebookUsername:String?
    
    // Phone
    var formattedPhone:String?
    var phone:String?
    
    // Another Social Medias
    var twitter:String?
    var instagram:String?
    
}
