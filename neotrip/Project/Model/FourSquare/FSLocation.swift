//
//  FSLocation.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSLocation : Codable {
    var lat:Double
    var lng:Double
    var distance:Int?
    
    var address:String?
    var crossStreet:String?
    var city:String?
    var state:String?
    var postalCode:String?
    var country:String
}
