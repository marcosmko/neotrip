//
//  FSResponse.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class FSResponse : Codable {
    
    // search
    var venues:[FSVenue]?
    
    // explore
    var groups:[FSGroup]?
    
    // venue
    var venue:FSVenue?
    
    // photos
    var photos:FSGroupPhoto?
    
}
