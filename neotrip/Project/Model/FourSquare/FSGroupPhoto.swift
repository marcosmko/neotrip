//
//  FSGroupPhoto.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 03/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSGroupPhoto : Codable {
    var count:Int
    var items:[FSPhoto]
}
