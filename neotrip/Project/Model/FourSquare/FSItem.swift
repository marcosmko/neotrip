//
//  FSItem.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSItem: Codable {
    var venue:FSVenue
}
