//
//  FSPhoto.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 03/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSPhoto : Codable {
    var id:String
    
    var height:Int
    var width:Int
    
    var prefix:String
    var suffix:String
}
