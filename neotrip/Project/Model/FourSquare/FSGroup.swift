//
//  FSGroup.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSGroup: Codable {
    var type:String
    var name:String
    
    var items:[FSItem]
}
