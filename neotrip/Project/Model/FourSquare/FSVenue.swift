//
//  FSVenue.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

struct FSVenue: Codable {
    var id:String
    var name:String
    
    var contact:FSContact
    var location:FSLocation
    
    var verified:Bool
}
