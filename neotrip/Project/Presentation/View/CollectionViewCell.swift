//
//  CollectionViewCell.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 03/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class CollectionViewCell: PinchCollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
