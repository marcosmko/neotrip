//
//  CityCollectionReusableView.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 21/09/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class CityCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var bottomLineView: UIView!
    
}
