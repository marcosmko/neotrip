//
//  TransportCollectionViewCell.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/27/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class TransportCollectionViewCell: PinchCollectionViewCell {
    
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var costLabel: UILabel!
    
}
