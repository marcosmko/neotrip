//
//  PinchCollectionView.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/23/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

@objc public protocol PinchCollectionViewDelegateFlowLayout: UICollectionViewDelegateFlowLayout {
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumHeightForItemAt indexPath: IndexPath) -> CGFloat
}

@objc public protocol PinchCollectionViewDataSource: UICollectionViewDataSource {
}

/// Interactive Collection View
public class PinchCollectionView: UICollectionView {
    
    public enum Mode { case compact, zooming, full }
    
    public private(set) var mode: Mode = .compact
    public private(set) var pinchIndexPath: IndexPath?
    public private(set) var height: CGFloat?
    
    private var baseHeight: CGFloat?
    private var offset: CGPoint?
    private var origin: CGPoint?
    
    /// Configure Collection View with gesture recognizers.
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        let gestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
        addGestureRecognizer(gestureRecognizer)
    }
    
    /// Returns a reusable cell object located by its identifier.
    /// Also, set the main view and detail view height.
    ///
    /// - Parameters:
    ///   - identifier: The reuse identifier for the specified cell. This parameter must not be nil.
    ///   - indexPath: The index path specifying the location of the cell. The data source receives this information when it is asked for the cell and should just pass it along. This method uses the index path to perform additional configuration based on the cell’s position in the collection view.
    /// - Returns: A valid UICollectionReusableView object.
    public override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        
        if let cell = cell as? Pinchable,
            let minHeight = _delegate?.collectionView(self, layout: collectionViewLayout, minimumHeightForItemAt: indexPath) {
            
            cell.mainViewHeightConstraint??.constant = minHeight
            cell.detailViewHeightConstraint??.constant = frame.height - minHeight
        }
        
        return cell
    }
    
    /// Delegate for PinchCollectionView with necessary functions for correct behaviour.
    var _delegate: PinchCollectionViewDelegateFlowLayout?
    override public var delegate: UICollectionViewDelegate? {
        get {
            return super.delegate
        }
        set {
            super.delegate = newValue
            _delegate = newValue as? PinchCollectionViewDelegateFlowLayout
        }
    }
    
    
    /// Function responsible for pinch behaviour.
    ///
    /// - Parameter recognizer: Current pinch gesture recognizer.
    @objc func handlePinch(recognizer: UIPinchGestureRecognizer) {
        
        switch recognizer.state {
        case .began:
            
            // Get cell to manipulate.
            let location: CGPoint = recognizer.location(in: self)
            if let newPinchIndexPath: IndexPath = indexPathForItem(at: location),
                let cell: UICollectionViewCell = cellForItem(at: newPinchIndexPath),
                let minHeight: CGFloat = _delegate?.collectionView(self, layout: collectionViewLayout, minimumHeightForItemAt: newPinchIndexPath) {
                
                // Set variables to interact.
                pinchIndexPath = newPinchIndexPath
                baseHeight = cell.frame.height
                height = baseHeight
                
                if (mode == .compact) {
                    mode = .zooming
                    
                    // Expanding cell.
                    reloadData()
                    offset = contentOffset
                    origin = cell.frame.origin
                    
                } else {
                    mode = .zooming
                    
                    // Contracting cell.
                    let numberOfItems: Int = self.numberOfItems(inSection: 0) - 1
                    
                    let startRect: CGRect = CGRect(origin: CGPoint.zero, size: frame.size)
                    let finalRect: CGRect = CGRect(origin: CGPoint(x: 0, y: CGFloat(numberOfItems) * minHeight - frame.height), size: frame.size)
                    let cellRect: CGRect = CGRect(x: 0, y: CGFloat(newPinchIndexPath.row) * minHeight, width: frame.width, height: minHeight)
                    
                    // Calculating origin and offset for contracting cell.
                    origin = CGPoint(x: 0, y: CGFloat(newPinchIndexPath.row) * minHeight)
                    if (startRect.contains(cellRect)) {
                        offset = startRect.origin
                    } else if (finalRect.contains(cellRect)) {
                        offset = finalRect.origin
                    } else {
                        offset = CGPoint(x: 0, y: CGFloat(newPinchIndexPath.row) * minHeight - minHeight)
                    }
                    
                }
                
            }
            
        case .changed:
            
            // Check if all needed variables exists.
            if let pinchIndexPath: IndexPath = pinchIndexPath,
                let cell: Pinchable = cellForItem(at: pinchIndexPath) as? PinchCollectionViewCell,
                let minHeight: CGFloat = _delegate?.collectionView(self, layout: collectionViewLayout, minimumHeightForItemAt: pinchIndexPath)  {
                
                // Calculate new height cell and update collection view.
                height = max(min(frame.height, baseHeight! * recognizer.scale), minHeight)
                UIView.performWithoutAnimation { performBatchUpdates({}) }
                
                // Calculate offset of collection view.
                let newOffset: CGFloat = (height! - minHeight) / (frame.height - minHeight)
                setContentOffset(CGPoint(x: 0, y: offset!.y + newOffset * (origin!.y - offset!.y)), animated: false)
                
                // Set alpha for detail view.
                cell.detailView!.alpha = newOffset
            }
            
        case .ended:
            
            // Check if all needed variables exists.
            if let pinchIndexPath: IndexPath = pinchIndexPath,
                let cell: Pinchable = cellForItem(at: pinchIndexPath) as? PinchCollectionViewCell,
                let height: CGFloat = height,
                let minHeight: CGFloat = _delegate?.collectionView(self, layout: collectionViewLayout, minimumHeightForItemAt: pinchIndexPath)  {
                
                // Check if collection view will expand or contract.
                let expand: Bool
                if abs(recognizer.velocity) > 0.5 {
                    if recognizer.velocity > 0 { expand = true }
                    else { expand = false }
                } else {
                    if height > self.frame.height / 2 { expand = true }
                    else { expand = false }
                }
                
                // Animate height changes.
                performBatchUpdates({
                    
                    if (expand) {
                        self.height = self.frame.height
                        self.selectItem(at: pinchIndexPath, animated: true, scrollPosition: .top)
                    } else {
                        self.height = minHeight
                        self.setContentOffset(CGPoint(x: 0, y: self.offset!.y), animated: true)
                    }
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.detailView!.alpha = (expand) ? 1.0 : 0.0
                    })
                    
                }) { bool in
                    
                    // Finish pinch interaction.
                    if (expand) {
                        self.mode = .full
                        self.isPagingEnabled = true
                        
                        self.reloadData()
                        self.selectItem(at: pinchIndexPath, animated: false, scrollPosition: .top)
                    } else {
                        self.mode = .compact
                        self.isPagingEnabled = false
                        
                        self.reloadData()
                    }
                    
                    // Set all variables to nil.
                    self.pinchIndexPath = nil
                    self.height = nil
                    self.baseHeight = nil
                    self.offset = nil
                    self.origin = nil
                }
                
            }
            
        default:
            break
        }
        
    }
    
    /// Not yet implemented.
    ///
    /// - Parameters:
    ///   - indexPath: IndexPath to expand.
    ///   - animated: Should animate or not.
    func expandItem(at indexPath: IndexPath?, animated: Bool) {
        
        performBatchUpdates({
            self.mode = .zooming
            
            self.height = self.frame.height
            
            self.selectItem(at: indexPath, animated: true, scrollPosition: .top)
        }) { (bool) in
            
            print(bool)
            self.mode = .full
            self.isPagingEnabled = true
            
            self.reloadData()
            self.selectItem(at: indexPath, animated: false, scrollPosition: .top)

            self.height = nil
        }
        
    }
    
}
