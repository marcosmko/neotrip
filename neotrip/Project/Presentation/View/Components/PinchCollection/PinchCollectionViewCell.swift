//
//  PinchCollectionViewCell.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/23/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

@objc protocol Pinchable {
    @objc optional weak var detailView: UIView! { get set }
    @objc optional weak var detailViewHeightConstraint: NSLayoutConstraint! { get set }
    @objc optional weak var mainViewHeightConstraint: NSLayoutConstraint! { get set }
}

class PinchCollectionViewCell: UICollectionViewCell, Pinchable {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let me: Pinchable  = self as Pinchable
        if (me.detailView == nil || me.detailViewHeightConstraint == nil || me.mainViewHeightConstraint == nil) {
            fatalError("Error: must implement.")
        }
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        layoutIfNeeded()
    }
    
}
