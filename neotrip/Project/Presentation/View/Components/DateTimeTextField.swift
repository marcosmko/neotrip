//
//  DateTimeTextField.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/15/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

@objc
protocol DateTimeTextFieldDelegate: UITextFieldDelegate {
    @objc optional func dateTimeTextField(_ textField: DateTimeTextField, didSelectDate date: Date)
}

public class DateTimeTextField: UITextField {
    
    fileprivate let datePicker: UIDatePicker = UIDatePicker()
    
    private weak var _delegate: DateTimeTextFieldDelegate?
    override public var delegate: UITextFieldDelegate? {
        get { return _delegate }
        set {
            _delegate = newValue as? DateTimeTextFieldDelegate
            super.delegate = newValue
        }
    }
    public var date: Date? {
        didSet {
            if let date = date, datePicker.date != date {
                datePicker.date = date
            }
        }
    }
    
    public var minimumDate: Date? {
        get { return datePicker.minimumDate }
        set { datePicker.minimumDate = newValue }
    }
    
    public var maximumDate: Date? {
        get { return datePicker.maximumDate }
        set { datePicker.maximumDate = newValue }
    }
    
    @IBInspectable public var type: Int = 0 {
        didSet {
            switch type {
            case 1:
                datePicker.datePickerMode = .date
            case 2:
                datePicker.datePickerMode = .time
            default:
                datePicker.datePickerMode = .dateAndTime
            }
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        addTarget(self, action: #selector(valueChanged), for: .editingDidBegin)
        datePicker.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        
        inputView = datePicker
    }
    
    @objc
    fileprivate func valueChanged(sender: UIDatePicker) {
        date = datePicker.date
        _delegate?.dateTimeTextField?(self, didSelectDate: date!)
        
        switch type {
        case 1:
            text = datePicker.date.description(dateStyle: .short, timeStyle: .none)
        case 2:
            text = datePicker.date.description(dateStyle: .none, timeStyle: .short)
        default:
            text = datePicker.date.description(dateStyle: .short, timeStyle: .short)
        }
    }
    
}
