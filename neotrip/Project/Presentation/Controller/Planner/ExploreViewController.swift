//
//  ExploreViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/27/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true)
    }
    
}

