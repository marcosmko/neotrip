//
//  AddCityViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/22/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

protocol AddCityDelegate {
    func didSelect(city: City)
}

class AddCityViewController: UIViewController {
    
    var cities: [City] = []
    var delegate: AddCityDelegate!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
}

extension AddCityViewController {
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func searchCity(_ sender: UITextField) {
        CityServices.search(with: sender.text!) { (cities, error) in
            if error == nil {
                self.cities = cities
                self.tableView.reloadData()
            } else {
                print("fuck this shit")
            }
        }
    }
    
}

extension AddCityViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = cities[indexPath.row].fullDescription
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city: City = cities[indexPath.row]
        
        CityServices.create(city) { (error) in
            if error == nil {
                self.delegate.didSelect(city: city)
            } else {
                
            }
            self.back(self)
        }
    }
    
}
