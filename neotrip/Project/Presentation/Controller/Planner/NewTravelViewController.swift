//
//  NewTravelViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/23/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

fileprivate enum Mode {
    case normal
    case localization
}

class NewTravelViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var startDateTextField: DateTimeTextField!
    @IBOutlet weak var endDateTextField: DateTimeTextField!
    
    var cities: [City] = []
    var city: City?
    @IBOutlet var textFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var localizeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var mode: Mode = .normal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
            nameTextField.text = "RESTLESS NEOTRIP"
        #endif
        
        if LocationServices.locationServicesAuthorized() {
            LocationServices.currentLocation({ (location, error) in
                if error == nil, let coordinate = location?.coordinate {
                    CityServices.search(latitude: coordinate.latitude, longitude: coordinate.longitude, { (city, error) in
                        if error == nil {
                            self.cityTextField.placeholder = city?.fullDescription
                            if self.city == nil {
                                self.city = city
                            }
                        }
                    })
                }
            })
        }
        
        setupUI()
    }
    
    fileprivate func setupUI() {
        tableView.alpha = 0.0
        startDateTextField.placeholder = Date().description(dateStyle: .short, timeStyle: .none)
        endDateTextField.placeholder = Date().description(dateStyle: .short, timeStyle: .none)
        localizeButton.isEnabled = LocationServices.locationServicesCanWork()
    }
    
    fileprivate func isValid() -> Bool {
        guard let name = nameTextField.text, !name.isEmpty else { return false }
        return true
    }
    
    fileprivate func showTableView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.alpha = 0.0
        }) { (_) in
            self.mode = .localization
            UIView.transition(with: self.localizeButton, duration: 0.6, options: [], animations: {
                self.localizeButton.setTitle("X", for: .normal)
            })
            UIView.animate(withDuration: 0.6, animations: {
                self.textFieldTopConstraint.isActive = false
                self.view.layoutIfNeeded()
            }) { (_) in
                UIView.animate(withDuration: 0.3) {
                    self.tableView.alpha = 1.0
                }
            }
        }
    }
    
    fileprivate func hideTableView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.alpha = 0.0
        }) { (_) in
            self.mode = .normal
            UIView.transition(with: self.localizeButton, duration: 0.6, options: [], animations: {
                self.localizeButton.setTitle("L", for: .normal)
            })
            UIView.animate(withDuration: 0.6, animations: {
                self.textFieldTopConstraint.isActive = true
                self.view.layoutIfNeeded()
            }) { (_) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.contentView.alpha = 1.0
                }, completion: { (_) in
                    self.cities = []
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editTravel",
            let vc = segue.destination as? PlannerViewController,
            let travel = sender as? Travel {
            vc.travel = travel
        }
    }
    
}

extension NewTravelViewController {
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func searchCity(_ sender: UITextField) {
        CityServices.search(with: sender.text!) { (cities, error) in
            if error == nil {
                self.cities = cities
                self.tableView.reloadData()
            } else {
                print("fuck this shit")
            }
        }
    }
    
    @IBAction func localize(_ sender: UIButton) {
        switch mode {
        case .normal:
            LocationServices.currentLocation({ (location, error) in
                if error == nil, let coordinate = location?.coordinate {
                    CityServices.search(latitude: coordinate.latitude, longitude: coordinate.longitude, { (city, error) in
                        self.cityTextField.text = city?.fullDescription
                    })
                } else {
                    print("fuck")
                }
            })
        case .localization:
            cityTextField.resignFirstResponder()
        }
        
    }
    
    @IBAction func create(_ sender: Any) {
        if isValid() {
            
            // create new travel.
            let travel: Travel = Travel()
            travel.name = nameTextField.text
            travel.startDate = startDateTextField.date
            travel.duration = 1000 * 60 * 60
            travel.origin = city
            
            // call services and perform segue to planner.
            TravelServices.create(travel) { (error) in
                self.performSegue(withIdentifier: "editTravel", sender: travel)
            }
        }
    }
    
}

extension NewTravelViewController: DateTimeTextFieldDelegate, UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == cityTextField {
            showTableView()
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == cityTextField {
            hideTableView()
        }
        return true
    }
    
    func dateTimeTextField(_ textField: DateTimeTextField, didSelectDate date: Date) {
        if textField == startDateTextField {
            endDateTextField.minimumDate = date
            endDateTextField.placeholder = date.description(dateStyle: .short, timeStyle: .none)
        } else if textField == endDateTextField {
            startDateTextField.maximumDate = date
            startDateTextField.placeholder = date.description(dateStyle: .short, timeStyle: .none)
        }
    }
    
}

extension NewTravelViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = cities[indexPath.row].fullDescription
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        city = cities[indexPath.row]
        cityTextField.text = cities[indexPath.row].fullDescription
        cityTextField.resignFirstResponder()
    }
    
}
