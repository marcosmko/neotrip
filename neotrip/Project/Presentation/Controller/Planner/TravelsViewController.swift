//
//  TravelsViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/23/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit
import CoreData

class TravelsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: PinchCollectionView!
    @IBOutlet weak var addButton: UIButton!
    
    var travels: [Travel] = []
    let minHeight: CGFloat = 100
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // fetch from database and reload.
        TravelServices.fetch { (error, travels) in
            if error == nil, let travels = travels {
                self.travels = travels
            }
            self.collectionView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editTravel",
            let vc = segue.destination as? PlannerViewController,
            let travel = sender as? Travel {
            vc.travel = travel
        }
    }
    
}

extension TravelsViewController {
    
    @IBAction func newTravel(_ sender: Any) {
        performSegue(withIdentifier: "newTravel", sender: sender)
    }
    
    @IBAction func unwind(segue: UIStoryboardSegue) {
    }
    
}

extension TravelsViewController: PinchCollectionViewDataSource, PinchCollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // must show travels, new travel and dummy view.
        return travels.count + 1 + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard (indexPath.row != travels.count + 1) else {
            // dummy view for correct pinch behaviour.
            return collectionView.dequeueReusableCell(withReuseIdentifier: "dummyCell", for: indexPath)
        }
        guard let collectionView: PinchCollectionView = collectionView as? PinchCollectionView else { fatalError("must implement pinch") }
        
        // dequeue cell and configure what should be displayed.
        let cell: TravelCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "travelCell", for: indexPath) as! TravelCollectionViewCell
        cell.topLineView.alpha = (indexPath.row == 0) ? 0.0 : 1.0
        cell.bottomLineView.alpha = (indexPath.row == travels.count) ? 0.0 : 1.0
        cell.detailView.alpha = (collectionView.mode == .full) ? 1.0 : 0.0
        cell.collectionView.collectionViewLayout.invalidateLayout()
        
        if (indexPath.row == travels.count) {
            // add new travel cell.
            cell.collectionView.isUserInteractionEnabled = false
            
            cell.plusLabel.text = "+"
            cell.nameLabel.text = "Adicione nova super fantástica experiência."
            cell.dateLabel.text = "Início? :) -> Fim! D: "
        } else {
            // travel cell.
            let travel: Travel = travels[indexPath.row]
            cell.collectionView.isUserInteractionEnabled = true
            
            cell.plusLabel.text = ""
            cell.nameLabel.text = travel.name
            if let startDate = travel.startDate {
                let endDate: Date = startDate.addingTimeInterval(travel.duration)
                
                if Calendar.current.isDate(startDate, inSameDayAs: endDate) {
                    cell.dateLabel.text = startDate.description
                } else {
                    cell.dateLabel.text = startDate.description + " - " + endDate.description
                }
            } else if (travel.duration > 0) {
                cell.dateLabel.text = travel.duration.time
            } else {
                cell.dateLabel.text = "Não setado ainda!"
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumHeightForItemAt indexPath: IndexPath) -> CGFloat {
        return minHeight
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // iterate over all visible cells to know if we must display add button.
        var visible = false
        collectionView.indexPathsForVisibleItems.forEach { (indexPath) in
            if indexPath.row == travels.count {
                visible = true
            }
        }
        UIView.animate(withDuration: 0.1) {
            self.addButton.alpha = (visible) ? 0.0 : 1.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let collectionView: PinchCollectionView = collectionView as? PinchCollectionView else { fatalError("must implement pinch") }
        let width: CGFloat = collectionView.frame.width
        
        // size for dummy view.
        if (indexPath.row == travels.count + 1) {
            switch collectionView.mode {
            case .compact, .full:
                return CGSize(width: width, height: 0)
            case .zooming:
                return CGSize(width: width, height: collectionView.frame.height)
            }
        }
        
        // Size for current zooming cell.
        switch collectionView.mode {
        case .compact:
            return CGSize(width: width, height: minHeight)
            
        case .zooming:
            if let pinchIndexPath = collectionView.pinchIndexPath, pinchIndexPath == indexPath,
                let height = collectionView.height
            {
                
                return CGSize(width: width, height: height)
            } else {
                return CGSize(width: width, height: minHeight)
            }
            
        case .full:
            return CGSize(width: width, height: collectionView.frame.height)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // perform segue to travel.
        if (indexPath.row == travels.count) {
            guard let collectionView = collectionView as? PinchCollectionView else { return }
            newTravel(collectionView)
        } else {
            performSegue(withIdentifier: "editTravel", sender: travels[indexPath.row])
        }
    }
    
}
