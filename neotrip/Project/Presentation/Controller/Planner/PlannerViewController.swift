//
//  PlannerViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/23/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit
import MapKit

class PlannerViewController: UIViewController {
    
    enum CityType {
        case origin, city, new
        
        init(tag: Int, cities: Int) {
            if tag == 0 {
                self = .origin
            } else if tag <= cities {
                self = .city
            } else {
                self = .new
            }
        }
    }
    
    var travel: Travel!
    let minHeight: CGFloat = 50
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var containerView: UIView!
    lazy var mapView: MKMapView = {
        let mapView: MKMapView = MKMapView()
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    } ()
    
    @IBOutlet weak var collectionView: PinchCollectionView!
    @IBOutlet weak var timelineLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameLabel.text = travel.name
        dateLabel.text = travel.startDate?.description(dateStyle: .short, timeStyle: .none)
    }
    
    @objc
    func handlePanGesture(recognizer: UIPanGestureRecognizer) {
        switch(recognizer.state) {
        case .began:
            if mapView.superview == nil {
                view.insertSubview(mapView, at: 0)
                
                let horizontalConstraints: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[mapView]-0-|", options: [], metrics: nil, views: ["mapView": mapView])
                let verticalConstraints: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:[menuView]-0-[mapView]-0-|", options: [], metrics: nil, views: ["mapView": mapView, "menuView": menuView])
                
                NSLayoutConstraint.activate(horizontalConstraints + verticalConstraints)
            }
            
        case .changed:
            let translation = recognizer.translation(in: view).x
            timelineLayoutConstraint.constant -= translation
            recognizer.setTranslation(CGPoint.zero, in: view)
            
        case .ended:
            let velocity: CGFloat = recognizer.velocity(in: view).x
            let constant: CGFloat
            
            if abs(velocity) > 5 {
                constant = (velocity > 0) ? 30 : view.frame.width
            } else {
                let showingMoreThanHalf = timelineLayoutConstraint.constant < view.frame.width/2
                constant = (showingMoreThanHalf) ? 30 : view.frame.width
            }
            
            timelineLayoutConstraint.constant = constant
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            })
            
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "city",
            let vc: AddCityViewController = segue.destination as? AddCityViewController {
            
            vc.delegate = self
        }
    }
    
}

extension PlannerViewController {
    
    @IBAction func showCity(_ sender: UIButton) {
        let cityType: CityType = CityType(tag: sender.tag, cities: travel.cities.count)
        
        switch cityType {
        case .origin:
            if let origin = travel.origin {
                
            } else {
                
            }
        // performSegue(withIdentifier: "origin", sender: self)
        case .city:
            performSegue(withIdentifier: "places", sender: self)
        case .new:
            performSegue(withIdentifier: "city", sender: self)
        }
    }
    
    @IBAction func back(_ sender: Any) {
        performSegue(withIdentifier: "showRoot", sender: self)
    }
    
}

extension PlannerViewController: AddCityDelegate {
    
    func didSelect(city: City) {
        travel.add(city)
        TravelServices.update(travel) { (error) in
            if error == nil {
                self.collectionView.reloadData()
            } else {
                
            }
        }
    }
    
}

extension PlannerViewController: PinchCollectionViewDataSource, PinchCollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return travel.cities.count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view: CityCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! CityCollectionReusableView
        view.addButton.tag = indexPath.section
        
        let cityType: CityType = CityType(tag: indexPath.section, cities: travel.cities.count)
        
        switch cityType {
        case .origin:
            view.addButton.alpha = 0.0
            
            if let origin = travel.origin {
                view.cityLabel.text = origin.fullDescription
            } else {
                view.cityLabel.text = "Cidade de Origem não selecionada..."
            }
        case .city:
            view.addButton.alpha = 1.0
            
            let cities: [City] = travel.cities.array as! [City]
            view.cityLabel.text = cities[indexPath.section - 1].fullDescription
        case .new:
            view.addButton.alpha = 1.0
            view.cityLabel.text = "Adicione nova cidade"
        }
        
        view.topLineView.alpha = 0.0
        view.bottomLineView.alpha = 0.0
        
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // must show travels, new travel and dummy view.
        let cityType: CityType = CityType(tag: section, cities: travel.cities.count)
        
        switch cityType {
        case .origin:
            return 1
        case .city:
            let city: City = travel.cities.object(at: section-1) as! City
            let places: Int = city.places.count
            let transports: Int = city.transports.count
            
            return max(1, places + transports)
        case .new:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let collectionView: PinchCollectionView = collectionView as? PinchCollectionView else { fatalError("must implement pinch") }
        let cell: PinchCollectionViewCell
        
        let cityType: CityType = CityType(tag: indexPath.section, cities: travel.cities.count)
        
        switch cityType {
        case .origin:
            let emptyCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emptyCell", for: indexPath)
            return emptyCell
        case .city:
            let city: City = travel.cities.object(at: indexPath.section-1) as! City
            let places: Int = city.places.count
            let transports: Int = city.transports.count
            
            if places + transports == 0 {
                let emptyCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emptyCell", for: indexPath)
                return emptyCell
            } else if indexPath.row < transports {
                let transportCell: TransportCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "transportCell", for: indexPath) as! TransportCollectionViewCell
                let transport: Transport = city.places.object(at: indexPath.row) as! Transport
                
                transportCell.nameLabel.text = "Transporte, Preço, Data"
                cell = transportCell
            } else {
                let placeCell: PlaceCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "placeCell", for: indexPath) as! PlaceCollectionViewCell
                let place: Place = city.places.object(at: indexPath.row - transports) as! Place
                
                placeCell.nameLabel.text = place.name
                cell = placeCell
            }
            
            (cell as Pinchable).detailView!.alpha = (collectionView.mode == .full) ? 1.0 : 0.0
            return cell
        case .new:
            fatalError("não era pra passar por aqui...")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumHeightForItemAt indexPath: IndexPath) -> CGFloat {
        return minHeight
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let collectionView: PinchCollectionView = collectionView as? PinchCollectionView else { fatalError("must implement pinch") }
        let width: CGFloat = collectionView.frame.width
        
        switch collectionView.mode {
        case .compact:
            return CGSize(width: width, height: minHeight)
            
        case .zooming:
            if let pinchIndexPath = collectionView.pinchIndexPath, pinchIndexPath == indexPath,
                let height = collectionView.height
            {
                
                return CGSize(width: width, height: height)
            } else {
                return CGSize(width: width, height: minHeight)
            }
            
        case .full:
            return CGSize(width: width, height: collectionView.frame.height)
            
        }
        
    }
    
}

