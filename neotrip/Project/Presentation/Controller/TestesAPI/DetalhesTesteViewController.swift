//
//  DetalhesTesteViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class DetalhesTesteViewController: UIViewController {
    
    var city:City!
    
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var searchTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CityServices.details(place_id: city.googlePlaceId!) { city in
            DispatchQueue.main.async {
                self.labels[0].text = city.googlePlaceId
                self.labels[1].text = city.name
                self.labels[2].text = city.fullDescription
                
                self.labels[3].text = city.geometry?.location.lat.description
                self.labels[4].text = city.geometry?.location.lng.description
                
                self.city = city
            }
        }
    }
    
    @IBAction func go(_ sender: Any) {
        FourSquareServices.explore(city: city, query: textField.text!) { venues in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "places", sender: venues)
            }
        }
    }
    
    @IBAction func search(_ sender: Any) {
        FourSquareServices.search(city: city, query: searchTextField.text!) { venues in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "places", sender: venues)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "places") {
            let vc = segue.destination as! PlacesViewController
            vc.venues = sender as! [FSVenue]
        }
    }
    
}
