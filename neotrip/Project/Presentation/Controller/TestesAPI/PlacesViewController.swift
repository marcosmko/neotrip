//
//  PlacesViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class PlacesViewController: UIViewController {
    
    var venues:[FSVenue]!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detail") {
            let vc = segue.destination as! DetalhesPlaceViewController
            vc.venue = venues[(sender as! IndexPath).row]
        }
    }
    
}

extension PlacesViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = venues[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "detail", sender: indexPath)
    }
    
}
