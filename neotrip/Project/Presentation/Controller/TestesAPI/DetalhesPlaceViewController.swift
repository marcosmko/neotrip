//
//  DetalhesPlaceViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 03/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit
import SDWebImage

class DetalhesPlaceViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var venue:FSVenue!
    var photos:[FSPhoto] = []
    @IBOutlet var labels: [UILabel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labels[0].text = venue.id
        FourSquareServices.details(venueID: venue.id) { (venue) in
            self.venue = venue
            
            DispatchQueue.main.async {
                self.labels[1].text = venue.name
            }
        }
        
        FourSquareServices.photos(venueID: venue.id) { (photos) in
            DispatchQueue.main.async {
                self.photos = photos
                self.collectionView.reloadData()
            }
        }
    }

}

extension DetalhesPlaceViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        let photo = photos[indexPath.row]
        
        cell.imageView.sd_cancelCurrentImageLoad()
        cell.imageView.sd_setImage(with: FourSquareServices.photo(photo: photo), placeholderImage: nil, options: [])
        
        return cell
    }
    
}
