//
//  TestesViewController.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

class TestesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var cities:[City] = []
    
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func get(_ sender: Any) {
        CityServices.find(query: textField.text!) { cities in
            self.cities = cities
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detail") {
            let vc = segue.destination as! DetalhesTesteViewController
            vc.city = cities[(sender as! IndexPath).row]
        }
    }

}

extension TestesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = cities[indexPath.row].fullDescription
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "detail", sender: indexPath)
    }
    
}
