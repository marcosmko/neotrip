//
//  URLSessionExtension.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/13/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

extension URLSession {
    
    public static func performSynchronousRequest(_ request: URLRequest, task: inout URLSessionDataTask?) throws -> (Data?, HTTPURLResponse?, Error?) {
        task?.cancel()
        
        var serverData: Data?
        var serverResponse: URLResponse?
        var serverError: Error?
        
        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
        
        task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error -> () in
            
            serverData = data
            serverResponse = response
            serverError = error
            
            semaphore.signal()
        })
        task!.resume()
        
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return (serverData, serverResponse as? HTTPURLResponse, serverError)
    }
    
}

