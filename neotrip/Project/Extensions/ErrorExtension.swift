//
//  ErrorExtension.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/18/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
