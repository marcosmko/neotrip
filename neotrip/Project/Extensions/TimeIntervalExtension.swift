//
//  NSTimeInterval.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 23/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    public var seconds: Int {
        return Int(fmod(self/1000, 60))
    }
    
    public var minutes: Int {
        return Int(fmod(self/(1000*60), 60))
    }
    
    public var hours: Int {
        return Int(fmod(self/(1000*60*60), 60))
    }
    
    public var days: Int {
        return Int(fmod(self/(1000*60*60*24), 24))
    }
    
    public var time: String {
        if (days    > 0) { return days.description    + " dia"     + ((days    > 1) ? "s" : "") }
        if (hours   > 0) { return hours.description   + " hora"    + ((hours   > 1) ? "s" : "") }
        if (minutes > 0) { return minutes.description + " minuto"  + ((minutes > 1) ? "s" : "") }
        if (seconds > 0) { return seconds.description + " segundo" + ((seconds > 1) ? "s" : "") }
        return ""
    }
    
}
