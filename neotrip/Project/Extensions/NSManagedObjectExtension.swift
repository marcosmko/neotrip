//
//  NSManagedObjectExtension.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/22/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import CoreData

extension NSManagedObject {
    
    func add(_ object: NSManagedObject, forKey key: String) {
        let set = mutableOrderedSetValue(forKey: key)
        set.add(object)
    }
    
    func remove(_ object: NSManagedObject, forKey key: String) {
        let set = mutableOrderedSetValue(forKey: key)
        set.remove(object)
    }
    
}
