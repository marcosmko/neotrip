//
//  Settings.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 23/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

extension Date {
    
    func description(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        return DateFormatter.localizedString(from: self, dateStyle: dateStyle, timeStyle: timeStyle)
    }
    
}
