//
//  UIViewExtension.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/23/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable open var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    @IBInspectable open var borderColor: UIColor? {
        get { return (layer.borderColor != nil) ? UIColor(cgColor: layer.borderColor!) : nil }
        set { layer.borderColor = newValue?.cgColor }
    }
    
    @IBInspectable open var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set {
            if (newValue < 1) {
                layer.cornerRadius = newValue * bounds.width
            } else {
                layer.cornerRadius = newValue
            }
        }
    }
    
}
