//
//  LocationServices.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 11/09/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation
import CoreLocation

public class LocationServices: NSObject {
    
    fileprivate static var me = LocationServices()
    fileprivate static let manager: CLLocationManager = CLLocationManager()
    
    fileprivate var semaphoreToRequestUse: DispatchSemaphore = DispatchSemaphore(value: 0)
    fileprivate var semaphoreToRequestLocation: DispatchSemaphore?
    
    fileprivate var location: CLLocation?
    
    fileprivate override init() {
        super.init()
        LocationServices.manager.delegate = self
    }
    
    public static func locationServicesCanWork() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .authorizedWhenInUse, .authorizedAlways:
                return true
            default:
                break
            }
        }
        return false
    }
    
    public static func locationServicesAuthorized() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .authorizedWhenInUse, .authorizedAlways:
                return true
            default:
                break
            }
        }
        return false
    }
    
    public static func currentLocation(_ completion: ((_ location: CLLocation?, _ error: Error?) -> Void)?) {
        let me = LocationServices.me
        
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error?
            var location: CLLocation?
            
            if CLLocationManager.locationServicesEnabled() {
                if CLLocationManager.authorizationStatus() == .notDetermined {
                    manager.requestWhenInUseAuthorization()
                    _ = me.semaphoreToRequestUse.wait(timeout: DispatchTime.distantFuture)
                }
                
                switch (CLLocationManager.authorizationStatus()) {
                case .notDetermined, .denied, .restricted:
                    raisedError = NeotripError.LocationFailure
                case .authorizedAlways, .authorizedWhenInUse:
                    me.semaphoreToRequestLocation = DispatchSemaphore(value: 0)
                    
                    manager.requestLocation()
                    _ = me.semaphoreToRequestLocation!.wait(timeout: DispatchTime.distantFuture)
                    
                    if me.location == nil {
                        raisedError = NeotripError.LocationFailure
                    } else {
                        location = me.location
                    }
                }
                
            } else {
                raisedError = NeotripError.LocationFailure
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(location, raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }
    
}

extension LocationServices: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined { semaphoreToRequestUse.signal() }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first
        semaphoreToRequestLocation?.signal()
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        semaphoreToRequestLocation?.signal()
    }
    
}
