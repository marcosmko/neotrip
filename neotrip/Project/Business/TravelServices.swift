//
//  TravelServices.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/24/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class TravelServices {
    
    /// Private initializer to avoid instantiation
    fileprivate init() { }
    
    static func create(_ travel: Travel, _ completion: ((_ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error? = nil
            
            do {
                if let city = travel.origin, city.managedObjectContext == nil {
                    try CityDAO.create(city)
                }
                try TravelDAO.create(travel)
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }
    
    static func update(_ travel: Travel, _ completion: ((_ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error? = nil
            
            do {
                try TravelDAO.update(travel)
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: {completion(raisedError)})
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }
    
    static func fetch(_ completion: ((_ error: Error?, _ travels: [Travel]?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error? = nil
            var travels: [Travel]?
            
            do {
                travels = try TravelDAO.fetch()
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(raisedError, travels) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }
    
}
