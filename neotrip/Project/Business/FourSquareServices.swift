//
//  FourSquare.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation
import UIKit

//do {
//    //                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
//    let json = try JSONSerialization.jsonObject(with: data as! Data, options: [])
//    print(json)
//} catch {
//    fatalError()
//}

class FourSquareServices {
    
    class func search(city:City, query:String, completion: @escaping ([FSVenue]) -> ()) {
        guard let location = city.geometry?.location else {
            return
        }
        
        var parameters = ["ll":"\(location.lat),\(location.lng)"]
        parameters["query"] = query
        parameters["section"] = "trending"
        
        RestAPI.get(endpoint: "/foursquare/search", parameters: parameters)
            .end { (data, response, error) in
                
                let decoder = JSONDecoder()
                do {
                    let serverResponse = try decoder.decode(FSServerResponse.self, from: data as! Data)
                    print(serverResponse)
                    
                    if let venues = serverResponse.response?.venues {
                        completion(venues)
                    }
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                }
                
                
        }
    }
    
    class func explore(city:City, query:String, completion: @escaping ([FSVenue]) -> ()) {
        guard let location = city.geometry?.location else {
            return
        }
        
        var parameters = ["ll":"\(location.lat),\(location.lng)"]
        parameters["query"] = query
        parameters["section"] = "trending"
        
        RestAPI.get(endpoint: "/foursquare/explore", parameters: parameters)
            .end { (data, response, error) in
                
                let decoder = JSONDecoder()
                do {
                    let serverResponse = try decoder.decode(FSServerResponse.self, from: data as! Data)
                    print(serverResponse)
                    
                    if let groups = serverResponse.response?.groups,
                        groups.count > 0 {
                        
                        var venues:[FSVenue] = []
                        groups[0].items.forEach({ (item) in
                            venues.append(item.venue)
                        })
                        completion(venues)
                    }
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                }
                
                
        }
    }
    
    class func details(venueID:String, completion: @escaping (FSVenue) -> ()) {
        
        RestAPI.get(endpoint: "/foursquare/\(venueID)")
            .end { (data, response, error) in
                
                let decoder = JSONDecoder()
                do {
                    let serverResponse = try decoder.decode(FSServerResponse.self, from: data as! Data)
                    print(serverResponse)
                    
                    if let venue = serverResponse.response?.venue {
                        completion(venue)
                    }
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                }
                
        }
    }
    
    class func photos(venueID:String, completion: @escaping ([FSPhoto]) -> ()) {
        
        RestAPI.get(endpoint: "/foursquare/\(venueID)/photos")
            .end { (data, response, error) in
                
                let decoder = JSONDecoder()
                do {
                    let serverResponse = try decoder.decode(FSServerResponse.self, from: data as! Data)
                    print(serverResponse)
                    
                    if let photos = serverResponse.response?.photos?.items {
                        completion(photos)
                    }
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                }
                
                
        }
    }
    
    class func photo(photo:FSPhoto) -> URL? {
        let url = URL(string: photo.prefix + "original" + photo.suffix)
        return url
    }
    
}
