//
//  CityServices.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 01/08/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class CityServices {
    
    static func create(_ city: City, _ completion: ((_ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error? = nil
            
            do {
                try CityDAO.create(city)
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }
    
    static func search(with query: String, _ completion: ((_ cities: [City], _ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation()
        
        blockForExecutionInBackground.addExecutionBlock {
            msleep(200)
            if blockForExecutionInBackground.isCancelled { return }
            
            var cities: [City] = []
            var raisedError: Error? = nil
            
            do {
                cities = try Server.searchCity(with: query)
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(cities, raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        }
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.single)
    }
    
    static func search(latitude: Double, longitude: Double, _ completion: ((_ city: City?, _ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation()
        
        blockForExecutionInBackground.addExecutionBlock {            
            var city: City?
            var raisedError: Error? = nil
            
            do {
                city = try Server.searchCity(latitude: latitude, longitude: longitude)
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(city, raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        }
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }
    
    class func find(query:String, completion: @escaping ([City]) -> ()) {
        var parameters = ["input":query]
        if let locale = locale() {
            parameters["language"] = locale
        }
        
        RestAPI.get(endpoint: "/cities", parameters: parameters)
            .end { (data, response, error) in
                
                let decoder = JSONDecoder()
                do {
                    struct CityArray : Codable {
                        var cities:[City]
                    }
                    
                    let serverResponse = try decoder.decode(CityArray.self, from: data as! Data)
//                    for venue in serverResponse.cities {
//                        print(venue.description)
//                    }
                    
                    completion(serverResponse.cities)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                }
                
                
        }
    }
    
    class func details(place_id:String, completion: @escaping (City) -> ()) {
        var parameters:[String:String] = [:]
        if let locale = locale() {
            parameters["language"] = locale
        }
        
        RestAPI.get(endpoint: "/cities/\(place_id)", parameters: parameters)
            .end { (data, response, error) in
                
                do {
                     print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    let json = try JSONSerialization.jsonObject(with: data as! Data, options: [])
                    print(json)
                } catch {
                    fatalError()
                }
                
                let decoder = JSONDecoder()
                do {

                    let serverResponse = try decoder.decode(City.self, from: data as! Data)
                    completion(serverResponse)
                    
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                }
                
                
        }
    }
    
}
