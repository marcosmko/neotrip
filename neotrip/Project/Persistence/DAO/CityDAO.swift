//
//  CityDAO.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/15/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class CityDAO {
    
    static func create(_ city: City) throws {
        do {
            CoreDataManager.sharedInstance.persistentContainer.viewContext.insert(city)
            try CoreDataManager.sharedInstance.persistentContainer.viewContext.save()
        } catch {
            throw NeotripError.DatabaseFailure
        }
    }
    
}
