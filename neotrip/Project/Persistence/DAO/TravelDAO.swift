//
//  TravelDAO.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/24/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class TravelDAO {
    
    static func create(_ travel: Travel) throws {
        do {
            CoreDataManager.sharedInstance.persistentContainer.viewContext.insert(travel)
            try CoreDataManager.sharedInstance.persistentContainer.viewContext.save()
        } catch {
            throw NeotripError.DatabaseFailure
        }
    }
    
    static func update(_ travel: Travel) throws {
        do {
            try CoreDataManager.sharedInstance.persistentContainer.viewContext.save()
        } catch {
            throw NeotripError.DatabaseFailure
        }
    }
    
    static func fetch() throws -> [Travel] {
        let moc = CoreDataManager.sharedInstance.persistentContainer.viewContext
        do {
            let fetchedTravels = try moc.fetch(Travel.fetchRequest()) as! [Travel]
            return fetchedTravels
        } catch {
            throw NeotripError.DatabaseFailure
        }
    }
    
}
