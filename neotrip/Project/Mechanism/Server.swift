//
//  Server.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 9/13/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class Server {
    
    fileprivate init() {
    }
    
    fileprivate static var searchTask: URLSessionDataTask?
    static func searchCity(with query: String) throws -> [City] {
        var parameters = ["input": query]
        if let locale = locale() {
            parameters["language"] = locale
        }
        
        let request = try createURLRequest(method: "GET", endpoint: "cities", parameters: parameters)
        let (data, response, error): (Data?, HTTPURLResponse?, Error?) = try URLSession.performSynchronousRequest(request, task: &searchTask)
        
        if let error = error, error.code != -999 {
            throw error
            
        } else if response?.statusCode == 200, let data = data {
            
            let decoder: JSONDecoder = JSONDecoder()
            struct CityArray: Codable {
                var cities: [City]
            }
            let serverResponse = try decoder.decode(CityArray.self, from: data)
            
            return serverResponse.cities
            
        } else { throw NeotripError.ServerFailure }
        
    }
    static func searchCity(latitude: Double, longitude: Double) throws -> City {
        var parameters = ["latlng": "\(latitude),\(longitude)"]
        if let locale = locale() {
            parameters["language"] = locale
        }
        
        let request = try createURLRequest(method: "GET", endpoint: "citieslookup", parameters: parameters)
        let (data, response, error): (Data?, HTTPURLResponse?, Error?) = try URLSession.performSynchronousRequest(request, task: &searchTask)
        
        if let error = error, error.code != -999 {
            throw error
            
        } else if response?.statusCode == 200, let data = data {

            let decoder: JSONDecoder = JSONDecoder()
            let serverResponse = try decoder.decode(City.self, from: data)
            
            return serverResponse
            
        } else { throw NeotripError.ServerFailure }
        
    }
    
    fileprivate static func createURLRequest(method: String, endpoint: String, parameters: [String: Any]? = nil/*, payload: JSON? = nil*/) throws -> URLRequest {
        // url request to be configured
        let url: URL
        if let parameters: String = parameters?.map({ (key, value) -> String in
            func addingPercentEncoding(value: String) -> String {
                return value.addingPercentEncoding(withAllowedCharacters:
                    CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.,_~")
                    )!
            }
            
            let percentEscapedKey: String = addingPercentEncoding(value: key)
            let percentEscapedValue: String = addingPercentEncoding(value: (value as! String))
            
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }).joined(separator: "&") {
            // with params
            url = URL(string: Constants.NeotripServer.baseURL + endpoint + "?\(parameters)")!
        } else {
            url = URL(string: Constants.NeotripServer.baseURL + endpoint)!
        }
        
        var urlRequest = URLRequest(url: url)
        // configure authentication header
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // if a payload is provided, set it
        // if (payload != nil) {
        //     urlRequest.httpBody = try payload?.rawData()
        // }
        
        return urlRequest
    }
    
}
