//
//  RestAPI.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 31/07/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

public class RestAPI {
    
    enum RestAPIApplication { case json, www }
    
    let webserver = "http://api.localhost.com:3000/v1/"
    
    typealias Headers = Dictionary<String, String>
    typealias Parameters = Dictionary<String, Any>
    typealias Response = (Data?, URLResponse?, Error?) -> Swift.Void
    
    /**
     * Members
     */
    
    var request: URLRequest
    var session: URLSessionTask?
    
    var operationQueue: OperationQueue?
    
    /**
     * Initialize
     */
    
    init(method: String, endpoint: String, headers: Headers?, parameters: Parameters? = nil) {
        if let parameters: String = parameters?.map({ (key, value) -> String in
            func addingPercentEncoding(value: String) -> String {
                return value.addingPercentEncoding(withAllowedCharacters:
                    CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.,_~")
                    )!
            }
            
            let percentEscapedKey: String = addingPercentEncoding(value: key)
            let percentEscapedValue: String = addingPercentEncoding(value: (value as! String))
            
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }).joined(separator: "&") {
            // with params
            self.request = URLRequest(url: URL(string: "\(webserver)\(endpoint)?\(parameters)")!)
        } else {
            self.request = URLRequest(url: URL(string: "\(webserver)\(endpoint)")!)
        }
        
        self.request.httpMethod = method
        self.request.allHTTPHeaderFields = headers
    }
    
    /**
     * GET
     */
    
    class func get(endpoint: String,
                   headers: Headers? = nil,
                   parameters: Parameters? = nil
        ) -> RestAPI {
        return RestAPI(method: "GET", endpoint: endpoint, headers: headers, parameters: parameters)
    }
    
    /**
     * POST
     */
    
    class func post(endpoint: String,
                    headers: Headers? = nil,
                    parameters: Parameters? = nil
        ) -> RestAPI {
        return RestAPI(method: "POST", endpoint: endpoint, headers: headers, parameters: parameters)
    }
    
    /**
     * PUT
     */
    
    class func put(endpoint: String,
                   headers: Headers? = nil,
                   parameters: Parameters? = nil
        ) -> RestAPI {
        return RestAPI(method: "PUT", endpoint: endpoint, headers: headers, parameters: parameters)
    }
    
    /**
     * DELETE
     */
    
    class func delete(endpoint: String,
                      headers: Headers? = nil,
                      parameters: Parameters? = nil
        ) -> RestAPI {
        return RestAPI(method: "DELETE", endpoint: endpoint, headers: headers, parameters: parameters)
    }
    
    /**
     * Methods
     */
    
    func set(operationQueue: OperationQueue) -> RestAPI {
        self.operationQueue = operationQueue
        return self
    }
    
    func set(header: String, value: String) -> RestAPI {
        self.request.setValue(value, forHTTPHeaderField: header)
        return self
    }
    
    func send(data: [String:Any], application: RestAPIApplication) -> RestAPI {
        
        switch application {
        case .json:
            
            _ = self.set(header: "Content-Type", value: "application/json")
            
            let json: Data?
            do {
                json = try JSONSerialization.data(withJSONObject: data, options: [])
                // print(NSString(data: json!, encoding: String.Encoding.utf8.rawValue)!)
            } catch _ as NSError {
                json = nil
            }
            self.request.httpBody = json
            
        case .www:
            
            _ = self.set(header: "Content-Type", value: "application/x-www-form-urlencoded")
            
            let body: String = data.map({ (key, value) -> String in
                return "\(key)=\(value)"
            }).joined(separator: "&")
            self.request.httpBody = body.data(using: String.Encoding.utf8)
            
        }
        
        return self
    }
    
    func end(done: @escaping Response) -> RestAPI {
        session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: operationQueue)
            .dataTask(with: self.request) { (data, response, error) in
                
                done(data, response as? HTTPURLResponse, error)
        }
        session?.resume()
        
        return self
    }
    
    func cancel() -> RestAPI {
        session?.cancel()
        return self
    }
    
    /**
     *  Function to upload image & data using POST request
     *
     *  @param image:NSData       image data of type NSData
     *  @param fieldName:String   field name for uploading image
     *  @param data:RequestData?  optional value of type Dictionary<String,AnyObject>
     *
     *  @return self instance to support function chaining
     */
    func data(image:NSData) -> RestAPI {
        if(image.length > 0 && self.request.httpMethod == "POST") {
            let body: NSMutableData = NSMutableData()
            
            let fname: String = "img.jpg"
            let mimetype: String = "image/jpg"
            
            //define the data post parameter
            let boundary: String = "Boundary-\(NSUUID().uuidString)"
            
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image as Data)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
        }
        return self
    }
    
}
