//
//  QueueManager.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 8/24/17.
//  Copyright © 2017 NeoTrip. All rights reserved.
//

import Foundation

class QueueManager {
    
    // Supported queues
    enum QueueType { case main, concurrent, serial, single }
    
    /// Queue used to serial operations
    fileprivate var serialQueue: OperationQueue?
    
    /// Queue used to concurrent operations
    fileprivate var concurrentQueue: OperationQueue?
    
    /// Queue used to concurrent operations
    fileprivate var singleQueue: OperationQueue?
    
    /// Queue manager singleton instance
    static let sharedInstance: QueueManager = QueueManager()
    
    /// Private initializer used to create and configure internal queues
    fileprivate init() {
        // initialize & configure serial queue
        serialQueue = OperationQueue()
        serialQueue?.maxConcurrentOperationCount = 1
        
        // initialize & configure concurrent queue
        concurrentQueue = OperationQueue()
        
        // initialize & configure single queue
        singleQueue = SingleOperationQueue()
    }
    
    /// Function responsible for executing a block of code in a particular queue
    /// - params:
    ///     - NSBlockOperation: block operation to be executed
    ///     - QueueType: queue where the operation will be executed
    func executeBlock(_ blockOperation: BlockOperation, queueType: QueueType) {
        // get queue where operation will be executed
        let queue: OperationQueue = self.getQueue(queueType)
        
        // execute operation
        queue.addOperation(blockOperation)
    }
    
    /// Function responsible for returning a specifi queue
    /// params:
    ///     - QueueType: desired queue
    /// returns: queue in according to the given param
    func getQueue(_ queueType: QueueType) -> OperationQueue {
        // queue to be returned
        var queueToBeReturned: OperationQueue? = nil
        
        // decide which queue
        switch queueType {
        case .concurrent:
            queueToBeReturned = self.concurrentQueue
        case .main:
            queueToBeReturned = OperationQueue.main
        case .serial:
            queueToBeReturned = self.serialQueue
        case .single:
            queueToBeReturned = self.singleQueue
        }
        
        return queueToBeReturned!
    }
}

